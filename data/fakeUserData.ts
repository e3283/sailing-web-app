import { User } from "../pages/users/profile";

const fakeUserData: User = {
    first_name: "Salvatore",
    last_name: "Schönborn",
    email: "salvatore.schonborn@example.com",
}

export default fakeUserData