export default [
    // Instagram eurilca
    {
        img: "https://eurilca.org/wp-content/uploads/2021/10/EurILCA-logo-2022-regular.png",
        title: "Instagram",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. At tellus at urna condimentum mattis. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat.",
        href: "https://www.instagram.com/eurilca/",
    },
    // Facebook eurilca
    {
        img: "https://eurilca.org/wp-content/uploads/2021/10/EurILCA-logo-2022-regular.png",
        title: "Facebook",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. At tellus at urna condimentum mattis. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat.",
        href: "https://www.facebook.com/eurilca",
    },
    // Twitter eurilca
    {
        img: "https://eurilca.org/wp-content/uploads/2021/10/EurILCA-logo-2022-regular.png",
        title: "Twitter",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. At tellus at urna condimentum mattis. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat.",
        href: "https://twitter.com/eurilca",
    }
]