import React, { FunctionComponent, useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";

import Dropdown from "../utils/Dropdown";
import Button from "../utils/Button";

import { getCookie, rmvCookie } from "../../utils/cookies";


// Common navigation links
const pages = [
    {
        name: "competitions",
        url: "/competitions",
    },
]

// User navigation links
const settings = [
    {
        name: "profile",
        url: "/users/profile"
    },
    {
        name: "dashboard",
        url: "/users/dashboard"
    }
]


const Navbar: FunctionComponent = () => {
    return (
        <nav className="fixed w-full flex flex-wrap items-center justify-between py-3 bg-black text-gray-200 shadow-lg navbar navbar-expand-lg navbar-light">
            <div className="container-fluid w-full flex flex-wrap items-center justify-between px-6">
                
                <button className="navbar-toggler text-gray-200 border-0 hover:shadow-none hover:no-underline py-2 px-2.5 bg-transparent focus:outline-none focus:ring-0 focus:shadow-none focus:no-underline" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" className="w-6" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path>
                    </svg>
                </button>

                <NavCollapse>
                    <Link href="/" passHref={true}>
                        <a className="text-xl text-white pr-2 font-semibold">Navbar</a>
                    </Link>

                    {/* <!-- Left links --> */}
                    <NavMenu>
                        {pages.map((page) => (
                            <NavItem key={page.name}>
                                <NavLink href={page.url}>{page.name}</NavLink>
                            </NavItem>
                        ))}
                    </NavMenu>
                    {/* <!-- Left links --> */}
                </NavCollapse>    


                {/* <!-- Collapsible wrapper --> */}

                {/* <!-- Right elements --> */}
                { getCookie("access_token") ? <UserNav/> : <LoginRegister/>}
                {/* <!-- Right elements --> */}

            </div>
        </nav>
    )
}

const LoginRegister: FunctionComponent = () => {
    return (
        <div>
            <Link href="/auth/login">
                <a><Button.Success className="mr-4">login</Button.Success></a>
            </Link>
            <Link href="/auth/register">
                <a><Button.Primary>register</Button.Primary></a>
            </Link>
        </div>
    )
}

const UserNav: FunctionComponent = (props) => { 

    const handleLogout = () => {
        rmvCookie("access_token")
        rmvCookie("token_type")

        window.location.reload()
    }

    return (
        <div className="flex items-center relative">
            <Dropdown>
                <Dropdown.Toggle>
                    <Image src="https://mdbootstrap.com/img/new/avatars/2.jpg" className="rounded-full" height={30} width={30} alt="" loading="lazy" />
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    {settings.map(setting => (
                        <Dropdown.Item key={setting.name}>
                            <Link href={setting.url} passHref={true}>
                                <Dropdown.Link>{setting.name}</Dropdown.Link>
                            </Link>
                        </Dropdown.Item>
                    ))}
                    <Dropdown.Item key={"logout"}>
                        <Dropdown.Link onClick={() => handleLogout()}>logout</Dropdown.Link>
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>   
        </div>
    )
}

const NavCollapse: FunctionComponent = ({children}) => {
    return (
        <div className="collapse navbar-collapse flex-grow items-center" id="navbarSupportedContent1">
            {children}
        </div>
    )
}

const NavMenu: FunctionComponent = ({children}) => {
    return (
        <ul className="navbar-nav flex flex-col pl-0 list-style-none mr-auto">
            {children}
        </ul>
    )
}

const NavItem: FunctionComponent = ({children}) => {
    return (
        <li className="nav-item p-2">
            {children}
        </li>
    )
}

const NavLink: FunctionComponent<{href: string}> = ({children, href}) => {
    return (
        <div className="nav-link text-white" >
            <Link href={href} passHref={true}>
                <a>{children}</a>
            </Link>
        </div>

    )
}

export default Navbar
