import React, { FunctionComponent, HTMLAttributes } from "react"

interface IMainProps extends HTMLAttributes<HTMLElement> {}
type MainProps = {} & IMainProps

const Main: FunctionComponent<MainProps> = (props) => {
    const { children, className, ...rest } = props;
    return (
        <main { ...rest } className={`${className} p-6`}>
            {children}
        </main>
    )
}

export default Main