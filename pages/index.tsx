import React, { FunctionComponent } from "react"

import type { NextPage } from 'next'

import Link from "next/link"
import Head from 'next/head'

import Heading from '../components/layout/Heading'
import Main from "../components/layout/Main"

import { H1, H2, H3, H5 } from '../components/utils/typography/Header'

import Card from "../components/Card"
import Button from "../components/utils/Button"
import Paragraph from "../components/utils/typography/Paragraph"

import HomeSocialMedia from "../data/HomeSocialMedia"
import Page from "../components/layout/Page"


const Home: NextPage = () => {
  return (
    <Page>

      <Head>
        <title>Sailing Application</title>
        <meta name="description" content="Sailing applicaiton with competitions" />
      </Head>

      <Heading>
            <H1>Sail away with us</H1>
            <H3>Start your voyage</H3>
            <Link href="/competitions" passHref={true}>
                <a><Button.Primary size="lg" outlined={true}>Get Started</Button.Primary></a>
            </Link>
      </Heading>

      <Main className="flex flex-col">

        {/* About content */}
        <About/>
  
        {/* Related content */}
        <H2>Related Content</H2>
        <hr/>
        {/* Social media profiles */}
        <div className="flex flex-wrap justify-evenly gap-y-8 mt-4">
          {HomeSocialMedia.map(social => (
            <Card className="" key={social.title}>
              <Card.Img src={social.img} alt={social.title}/>
              <Card.Body>
                <H5>{social.title}</H5>
                <Paragraph>{social.text}</Paragraph>
                  <Link href={social.href} passHref={true}>
                      <a target="_blank">
                        <Button.Primary>view page</Button.Primary>
                      </a>
                  </Link>
              </Card.Body>
            </Card>
          ))}
          {/* Social media profiles */}

        </div>
        {/* Related content */}

      </Main>

    </Page>
  )
}

const About: FunctionComponent = () => {
  return (
      <div className="mt-4">
      <H2>Our Venture</H2>
      <hr/>
      <Paragraph>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. At tellus at urna condimentum mattis. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat. Pretium aenean pharetra magna ac placerat vestibulum. Urna condimentum mattis pellentesque id nibh tortor. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Dolor sit amet consectetur adipiscing elit duis tristique sollicitudin nibh. Nisi scelerisque eu ultrices vitae auctor eu augue. Nisi lacus sed viverra tellus in hac habitasse. Purus ut faucibus pulvinar elementum integer enim neque volutpat ac. Pellentesque nec nam aliquam sem et tortor consequat id.
      </Paragraph>

      {/* Conditional paragraph in order to not have to much text on mobile screens */}
      <div className="hidden sm:block">
        <Paragraph>
          Elementum nibh tellus molestie nunc. Vitae ultricies leo integer malesuada nunc vel risus commodo viverra. In ante metus dictum at tempor commodo ullamcorper a. Suspendisse sed nisi lacus sed viverra tellus in hac habitasse. Dignissim convallis aenean et tortor at risus viverra adipiscing at. Posuere urna nec tincidunt praesent semper feugiat. Tempus iaculis urna id volutpat. Tincidunt eget nullam non nisi est sit amet. Tortor at auctor urna nunc id cursus metus aliquam. Tristique senectus et netus et malesuada fames ac. Viverra nibh cras pulvinar mattis nunc sed blandit. Scelerisque varius morbi enim nunc faucibus a pellentesque. Turpis tincidunt id aliquet risus feugiat in ante. Cursus mattis molestie a iaculis at erat pellentesque. Hac habitasse platea dictumst vestibulum. Montes nascetur ridiculus mus mauris vitae. Felis donec et odio pellentesque diam volutpat commodo. Scelerisque mauris pellentesque pulvinar pellentesque. Scelerisque varius morbi enim nunc.
        </Paragraph>
      </div>
    </div>
  )
}

export default Home

