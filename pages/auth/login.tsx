import React, { FormEvent, useEffect, useState } from "react"
import { NextPage } from "next"
import Main from "../../components/layout/Main"
import Page from "../../components/layout/Page"
import Form from "../../components/Form"
import Button from "../../components/utils/Button"
import { H1 } from "../../components/utils/typography/Header"
import { useRouter } from 'next/router'

import { setCookie } from "../../utils/cookies"


const Login: NextPage = () => {
    const [showPassword, setShowPassword] = useState(false)

    const [username, setUsername] = useState("")  // OAuth2 expects an username for authentication but we pass an email
    const [password, setPassword] = useState("")

    const router = useRouter()

    const handleLogin = async (event: FormEvent) => {
        event.preventDefault()

        const response = await fetch("http://localhost:8001/auth/token", {
            method: "POST",
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
            body: `grant_type=password&username=${username}&password=${password}&scope=me:read`,
        })
        
        if (response.ok) {
            const data: { access_token: string; token_type: string } = await response.json()

            setCookie('access_token', data.access_token, { path: '/' })
            setCookie('token_type', data.token_type, { path: '/' })

            router.push("/users/profile")
        } 
    }

    return (
        <Page>
            <Main>
                <H1>Login</H1>
                <hr/>
                <Form className="mt-4" onSubmit={(e) => handleLogin(e)}>
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Input type="email" placeholder="Email" value={username} onChange={(e) => setUsername(e.target.value)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Input type={ !showPassword ? "password" : "text"} placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                        <Form.Group>
                            <Form.CheckBox onChange={() => setShowPassword(!showPassword)}/>
                            <Form.Label>Show Password</Form.Label>
                        </Form.Group>
                    </Form.Group>
                    <Button.Success type="submit" size="lg">Login</Button.Success>
                </Form>
            </Main>
        </Page>
    )
}

export default Login