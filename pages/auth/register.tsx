import React, { useState, FormEvent } from "react"
import { NextPage } from "next"
import { H1 } from "../../components/utils/typography/Header"
import Main from "../../components/layout/Main"
import Page from "../../components/layout/Page"
import Form from "../../components/Form"
import Button from "../../components/utils/Button"
import { User } from "../users/profile"
import { useMutation } from "react-query"
import Alert from "../../components/utils/Alert"
import { useRouter } from "next/router"


const register = async (data: User & { password: string }) => {
    const response = await fetch(`http://127.0.0.1:8001/auth/register`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
    })

    return await response.json()
}


const Register: NextPage = () => {

    const router = useRouter()

    const { isSuccess, mutate } = useMutation(register, {
        onSuccess: () => {
            router.push("/auth/login")
        },
        retry: 3
    })

    const [showPassword, setShowPassword] = useState(false)

    const [first_name, setFirstName] = useState("")
    const [last_name, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const handleSubmit = (event: FormEvent) => {
        event.preventDefault()
        mutate({
            first_name,
            last_name,
            email,
            password,
        })
    }

    return (
        <Page>
            <Main>
                <H1>Register</H1>
                <hr/>
                {isSuccess ? <Alert.Success className="mt-4">Successfully Registered</Alert.Success> : null}
                <Form className="mt-4" onSubmit={(e) => handleSubmit(e)}>
                    <Form.Group>
                        <Form.Label>First name</Form.Label>
                        <Form.Input type="text" placeholder="First name"  value={first_name} onChange={(e) => setFirstName(e.target.value)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Last name</Form.Label>
                        <Form.Input type="text" placeholder="Last name" value={last_name} onChange={(e) => setLastName(e.target.value)}/>
                    </Form.Group>                    
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Input type="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Input type={ !showPassword ? "password" : "text"} placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                        <Form.Group>
                            <Form.CheckBox onChange={() => setShowPassword(!showPassword)}/>
                            <Form.Label>Show Password</Form.Label>
                        </Form.Group>
                    </Form.Group>
                    <Button.Success type="submit" size="lg">Register</Button.Success>
                </Form>
            </Main>
        </Page>
    )
}

export default Register