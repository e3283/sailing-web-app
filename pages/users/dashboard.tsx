import React, { useState } from "react"
import { NextPage } from "next"
import Page from "../../components/layout/Page"
import Main from "../../components/layout/Main"
import { H1, H2, H5 } from "../../components/utils/typography/Header"
import Button from "../../components/utils/Button"
import { useQuery } from "react-query"
import { Competition } from "../competitions"
import Card from "../../components/Card"
import Paragraph from "../../components/utils/typography/Paragraph"
import Link from "next/link"


enum Active {
    ORGANIZOR = "ORGANIZOR",
    OFFICIAL = "OFFICIAL",
    COMPETITOR = "COMPETITOR",
}

const fetchCompetitions = async () => {
    const response = await fetch("https://ghibliapi.herokuapp.com/films")
    if (!response.ok) {
        const status = response.statusText
        throw new Error(status)
    }
    return response.json()
}


const Dashboard: NextPage = () => {

    const { isLoading, isError, data: competitions } = useQuery<Competition[], Error>("competitions", fetchCompetitions)

    const [activeBtn, setActiveBtn] = useState(Active.ORGANIZOR)

    return (
        <Page>
            <Main>
                <div className="flex flex-wrap mb-1 sm:mb-0">
                    <H1 className="mr-auto">Dashboard</H1>
                    <div className="my-auto flex flex-wrap gap-4">
                        <Button.Primary className="w-52" onClick={() => setActiveBtn(Active.ORGANIZOR)} outlined={activeBtn === Active.ORGANIZOR ? true : false}>As Organizor</Button.Primary>
                        <Button.Primary className="w-52" onClick={() => setActiveBtn(Active.OFFICIAL)} outlined={activeBtn === Active.OFFICIAL ? true : false}>As Official</Button.Primary>
                        <Button.Primary className="w-52" onClick={() => setActiveBtn(Active.COMPETITOR)} outlined={activeBtn === Active.COMPETITOR ? true : false}>As Competitor</Button.Primary>
                    </div>
                </div>
                <hr/>
                <div className="flex flex-wrap justify-evenly gap-y-8 mt-4">
                {isLoading ? <H2 className="text-center">Loading...</H2> : null}
                {isError ? <H2 className="text-center">Something Went Wrong!</H2> : null}
                {competitions ? 
                        competitions.filter(competition => competition.title.length < activeBtn.length).map(competition => (
                            <Card key={competition.id}>
                                <Card.Img src={competition.img} alt={competition.title}/>
                                <Card.Body>
                                    <H5>{competition.title}</H5>
                                    <Paragraph>{competition.description.substring(0, 200)}...</Paragraph>
                                    <Link href={`/competitions/${competition.id}`} passHref={true}>
                                        <a>
                                            <Button.Link>view Competition</Button.Link>
                                        </a>
                                    </Link>
                                </Card.Body>
                            </Card>
                        ))
                        :
                        null
                    }
                </div>    
            </Main>
        </Page>
    )
}

export default Dashboard