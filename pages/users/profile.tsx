import React, { useEffect, useState } from "react"
import { NextPage } from "next"
import Page from "../../components/layout/Page"
import Main from "../../components/layout/Main"
import { H1 } from "../../components/utils/typography/Header"
import Form from "../../components/Form"
import { useRouter } from "next/router"

import { getCookie } from "../../utils/cookies"


export type User = {
    first_name: string
    last_name: string
    email: string
}

type Props = {
    user: User
}

const Profile: NextPage = () => {

    const router = useRouter()

    const [user, setUser] = useState<User | null>()

    useEffect(() => {
        fetchUser()
    }, [])

    const fetchUser = async () => {

        const access_token = getCookie("access_token")
        const token_type = getCookie("token_type")

        const response = await fetch("http://localhost:8001/users/me", {
            headers: {
                "Authorization": `${token_type} ${access_token}`,
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })

        if (response.status == 401) router.push("/auth/login")
        
        const user: User = await response.json()
        setUser(user)
    }

    return (
        <Page>
            <Main>
                <H1>Profile Page</H1>
                <hr/>
                <Form>
                    <Form.Group>
                        <Form.Label>First name</Form.Label>
                        <Form.Input disabled={true} value={user?.first_name}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Last name</Form.Label>
                        <Form.Input disabled={true} value={user?.last_name}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Input disabled={true} value={user?.email}/>
                    </Form.Group>
                </Form>
            </Main>
        </Page>
    )
}

export default Profile