import React, { FunctionComponent } from "react"

import type { NextPage } from 'next'
import { GetServerSideProps } from 'next'

import Link from "next/link"
import Main from "../../components/layout/Main"

import Button from "../../components/utils/Button"
import { H2, H5 } from "../../components/utils/typography/Header"
import Card from "../../components/Card"
import Paragraph from "../../components/utils/typography/Paragraph"
import Modal from "../../components/Modal"
import Form from "../../components/Form"
import Page from "../../components/layout/Page"


export type Competition = {
    id: string
    title: string
    description: string
    img: string
}

type Props = {
    competitions: Competition[]
}

const Competitions: NextPage<Props> = ({competitions}) => {
    return (
        <Page>
            <Main>
                <div className="flex flex-wrap mb-1 sm:mb-0">
                    <H2 className="mr-auto">Competitions</H2>
                    <Button.Success data-bs-toggle="modal" data-bs-target="#staticBackdrop" className="h-min w-40" size="lg">
                        create
                    </Button.Success>
                </div>
                <hr/>
                <div className="flex flex-wrap justify-evenly gap-y-8 mt-4">
                    {competitions.map(competition => (
                        <Card key={competition.id}>
                            <Card.Img src={competition.img} alt={competition.title}/>
                            <Card.Body>
                                <H5>{competition.title}</H5>
                                <Paragraph>{competition.description.substring(0, 200)}...</Paragraph>
                                <Link href={`/competitions/${competition.id}`} passHref={true}>
                                    <a>
                                        <Button.Link>view Competition</Button.Link>
                                    </a>
                                </Link>
                            </Card.Body>
                        </Card>
                    ))}
                </div>
            </Main>

            {/* Modal for creating competition */}
            <CreateModal />
        </Page>
    )
}


const CreateModal: FunctionComponent = () => {
    return (
        <Modal size="xl">
            <Modal.Header>
                <H2>Update Competition</H2>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group>
                        <Form.Label>Title</Form.Label>
                        <Form.Input type="text" placeholder="Title"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.TextArea placeholder="password" />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Country</Form.Label>
                        <Form.Select>
                            <option>Netherlands</option>
                        </Form.Select>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Sailing Club</Form.Label>
                        <Form.Input type="text"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Max Nr. Officials</Form.Label>
                        <Form.Input type="number" min={0} max={40}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Max Nr. Competitors</Form.Label>
                        <Form.Input type="number" min={0} max={100}/>
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button.Primary className="mr-auto">submit</Button.Primary>
                <Button.Danger data-bs-dismiss="modal" outlined={true}>close</Button.Danger>
            </Modal.Footer>
        </Modal>
    )
}

export const getServerSideProps: GetServerSideProps = async () => {
    const response = await fetch(
        "https://ghibliapi.herokuapp.com/films"
    )
    const competitions: Competition[] = await response.json()

    return {
        props: {
            competitions: competitions
        }, // will be passed to the page component as props
    }
}

export default Competitions