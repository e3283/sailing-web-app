import React, { FunctionComponent } from "react"

import type { NextPage } from 'next'
import { GetServerSideProps } from 'next'

import Main from "../../components/layout/Main"

import { H1, H2, H3 } from "../../components/utils/typography/Header"

import { Competition as CompetitionType } from "."
import Paragraph from "../../components/utils/typography/Paragraph"
import Button from "../../components/utils/Button"
import Heading from "../../components/layout/Heading"
import Modal from "../../components/Modal"
import Form from "../../components/Form"
import Page from "../../components/layout/Page"

type Props = {
    competition: CompetitionType
}

const tempBool = false

const Competition: NextPage<Props> = ({competition}) => {
    return (
        <Page>

            <Heading>
                <H1>{competition.title}</H1>
                <H3>Competition Location</H3>                   
                { tempBool ? 
                    <Button.Primary size="lg" outlined={true}>join competition</Button.Primary> 
                    :
                    <a target="_blank" href="#"><Button.Primary size="lg" outlined={true}>view results</Button.Primary></a>
                }   
            </Heading>
            
            <Main>
                <H2>Information</H2>
                <hr/>
                <Paragraph.Lead>{competition.description}</Paragraph.Lead>

                <Paragraph>Nr. Officials: 12/40</Paragraph>
                <Paragraph>Nr. Competitors: 40/100</Paragraph>

                <div className="flex justify-between mt-4">
                    <Button.Primary size="lg" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Update</Button.Primary> <Button.Danger size="lg" outlined={true}>Delete</Button.Danger>
                </div> 

            </Main>
            
            {/* Modal for updating competition */}
            <UpdateModal competition={competition} />

        </Page>
    )
}

const UpdateModal: FunctionComponent<Props> = ({ competition }) => {
    return (
        <Modal size="xl">
            <Modal.Header>
                <H2>Update Competition</H2>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group>
                        <Form.Label>Title</Form.Label>
                        <Form.Input type="text" placeholder="Title" value={competition.title} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.TextArea placeholder="password" value={competition.description}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Country</Form.Label>
                        <Form.Select>
                            <option>Netherlands</option>
                        </Form.Select>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Sailing Club</Form.Label>
                        <Form.Input type="text"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Max Nr. Officials</Form.Label>
                        <Form.Input type="number" min={0} max={40} value={12}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Max Nr. Competitors</Form.Label>
                        <Form.Input type="number" min={0} max={100} value={40}/>
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button.Primary className="mr-auto">submit</Button.Primary>
                <Button.Danger data-bs-dismiss="modal" outlined={true}>close</Button.Danger>
            </Modal.Footer>
        </Modal>
    )
}

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
    const response = await fetch(
        `https://ghibliapi.herokuapp.com/films/${params?.id}`
    )
    const competition: CompetitionType = await response.json()

    return {
        props: {
            competition: competition
        }, // will be passed to the page component as props
    }
}


export default Competition