import "../styles/globals.css"

import React, { useState } from "react"
import type { AppProps } from "next/app"
import { useEffect } from "react"
import Navbar from "../components/layout/Navbar"
import Footer from "../components/layout/Footer"
import { ReactQueryDevtools } from 'react-query/devtools'
import {
  QueryClient,
  QueryClientProvider,
} from 'react-query'

function MyApp({ Component, pageProps }: AppProps) {
  const [queryClient] = useState(() => new QueryClient());

  useEffect(() => {
    require("tw-elements")
  }, [])

  return (
        <QueryClientProvider client={queryClient}>
          <div className="flex flex-col h-screen">
            <Navbar/>
            <Component {...pageProps} />
            <Footer/>
          </div>
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
    )
}

export default MyApp
