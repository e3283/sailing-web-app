module.exports = {
    ci: {
        collect: {
            url: [
                "https://staging-sailing.herokuapp.com",
                "https://staging-sailing.herokuapp.com/competitions",
                "https://staging-sailing.herokuapp.com/competitions/2baf70d1-42bb-4437-b551-e5fed5a87abe",
                "https://staging-sailing.herokuapp.com/auth/login",
                "https://staging-sailing.herokuapp.com/auth/register",
            ],
            settings: {
                chromeFlags: "--no-sandbox",
            },
            numberOfRuns: 3
        },
        assert: {
            // assert options here
            preset: "lighthouse:no-pwa",
            assertions: {
                "categories:performance": ["error", {minScore: 0.9}],
                "categories:accessibility": ["error", {minScore: 0.9}],
                "categories:best-practices": ["error", {minScore: 0.9}],
                "categories:seo": ["error", {minScore: 0.9}],
            }
        },
        upload: {
            // upload options here
            target: "temporary-public-storage",
        },
    },
}