import Cookies, { CookieSetOptions } from 'universal-cookie';

const cookies = new Cookies();

export const getCookie = (name: string): string => cookies.get(name)

export const setCookie = (name: string, value: string, options?: CookieSetOptions): void => cookies.set(name, value, options)

export const rmvCookie = (name: string): void => cookies.remove(name)
